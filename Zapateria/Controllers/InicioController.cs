﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zapateria.Models;
using Zapateria.DAO;

namespace Zapateria.Controllers
{
    public class InicioController : Controller
    {
        // GET: Inicio
        public ActionResult vistaPrincipal()
        {
            return View("vistaPrincipal");
        }
        public ActionResult Login()
        {
           
            return View("Login");
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult login_(UsuarioModel usuario)
        {
            if (!string.IsNullOrWhiteSpace(usuario.correo) &&
        !string.IsNullOrWhiteSpace(usuario.contra))
            {
                LoginDAO login = new LoginDAO();
                
                UsuarioModel usu = login.verificar(usuario);
                if (usu != null)
                {

                    Session["usuario"] = usu;



                   
                            // administrador 1

                            return Redirect("~/Backend/GuardarZapato");
                        
                    // return Redirect("~/BackendAdmin/login");
                }
               

                //  Response.Write("<script>alert('Correo o contraseña incorrecta ')</script>");

                return Redirect("~/Inicio/Login");
            }
            else
            {
                ViewBag.Result = null;
                return View("Login");
            }
        }

        public ActionResult Mision()
        {
           
            return View("Mision");
        }
        public ActionResult Vision()
        {

            return View("vision");
        }
        public ActionResult Contacto()
        {

            return View("Contacto");
        }
    }
}