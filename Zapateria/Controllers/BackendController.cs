﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zapateria.Models;
using Zapateria.DAO;
using System.IO;

namespace Zapateria.Controllers
{
    public class BackendController : Controller
    {
        // GET: Backend
        public ActionResult InicioBackend()
        {
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];
                return View();
            }
            return View();
        }

        public ActionResult GuardarZapato()
        {
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];
                return View();
            }
            return View();
        }
        public ActionResult saveZapato(Zapatos zapats)
        {
            string filename;
            string extencion;

            ZapaDAL obj = new ZapaDAL();
            filename = Path.GetFileNameWithoutExtension(zapats.ImageFile.FileName);
            extencion = Path.GetExtension(zapats.ImageFile.FileName);
            filename = filename + DateTime.Now.ToString("yymmssfff") + extencion;
            // cursos.id_vendedor = ViewBag.vista.id;
            zapats.Imagen = "~/imagenes/" + filename;
            filename = Path.Combine(Server.MapPath("~/imagenes/"), filename);
            zapats.ImageFile.SaveAs(filename);





            int i = obj.agregar(zapats);
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];
                return View("GuardarZapato");
            }
            return View("GuardarZapato");
        }
        public ActionResult Create()
        {
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];
                return View();

            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(Zapatos collection)
        {
            try
            {
                // TODO: Add insert logic here

                if (ModelState.IsValid)
                {
                    string filename;
                    string extencion;

                    filename = Path.GetFileNameWithoutExtension(collection.ImageFile.FileName);
                    extencion = Path.GetExtension(collection.ImageFile.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extencion;
                    // cursos.id_vendedor = ViewBag.vista.id;
                    collection.Imagen = "~/imagenes/" + filename;
                    filename = Path.Combine(Server.MapPath("~/imagenes/"), filename);
                    collection.ImageFile.SaveAs(filename);

                    zapatosDAL.AgregarZapato(collection);
                    if (Session["usuario"] != null)
                    {
                        ViewBag.vista = (UsuarioModel)Session["usuario"];
                        return RedirectToAction("Index");

                    }
                    return View("Index");
                }
                else
                {
                    if (Session["usuario"] != null)
                {
                    ViewBag.vista = (UsuarioModel)Session["usuario"];
                    return View();

                }
                return View();
                }

            }
            catch(Exception e)
            {
                return View(e.Message);
            }
        }
        public ActionResult Edit(int id)
        {

             Zapatos o = zapatosDAL.ObtenerDatosZapatos(id);
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];

                return View(o);
            }
            
           
            return View();
        }
         ZapatosDAL zapatosDAL = new ZapatosDAL();
        public ActionResult Index()
        {
            ZapatosDAL zapatosDAL = new ZapatosDAL();
            List<Zapatos> lstempleados = new List<Zapatos>();
            lstempleados = zapatosDAL.ListarZapatos().ToList();
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];
                return View(lstempleados);
                
            }
            return View();

        }
        public ActionResult Details(int id)
        {
            Zapatos Oempledos = zapatosDAL.ObtenerDatosZapatos(id);
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];
                return View(Oempledos);

            }
          
            return View();

        }
        public ActionResult Delete(int id)
        {
           Zapatos o = zapatosDAL.ObtenerDatosZapatos(id);
            if (Session["usuario"] != null)
            {
                ViewBag.vista = (UsuarioModel)Session["usuario"];
                return View(o);

            }
           
            return View();
        }

        // POST: Empleado/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Zapatos collection)
        {
            try
            {
                // TODO: Add delete logic here
                zapatosDAL.BorrarZapato(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult cerrar_sesion()
        {
            Session.Remove("usuario");
            Session.Abandon();
            return Redirect("~/Inicio/vistaPrincipal");
        }
        [HttpPost]
        public ActionResult Edit(int id, Zapatos collection)
        {
            try
            {
                // TODO: Add update logic here

                if (ModelState.IsValid)
                {
                    string filename;
                    string extencion;
                    if (collection.ImageFile!=null )
                    {
                        filename = Path.GetFileNameWithoutExtension(collection.ImageFile.FileName);
                        extencion = Path.GetExtension(collection.ImageFile.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extencion;
                        // cursos.id_vendedor = ViewBag.vista.id;
                        collection.Imagen = "~/imagenes/" + filename;
                        filename = Path.Combine(Server.MapPath("~/imagenes/"), filename);
                        collection.ImageFile.SaveAs(filename);
                    }
                   

                    zapatosDAL.ModificarZapatos(collection);
                    if (Session["usuario"] != null)
                    {
                        ViewBag.vista = (UsuarioModel)Session["usuario"];

                        return RedirectToAction("Index");
                    }
                   
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                
                return View(e.Message);
            }
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult guardar_curso(CursosBO cursos)
        //{
        //    if (Session["usuario"] != null)
        //    {
        //        CursosDAO curso = new CursosDAO();
        //        string filename;
        //        string extencion;
        //        usuarioDAO vendedores = new usuarioDAO();

        //        ViewBag.vendedores = vendedores.mandar_lista_vendedores();
        //        ViewBag.vista = (usuarioBO)Session["usuario"];
        //        if (!string.IsNullOrWhiteSpace(cursos.nombre_curso) &&
        //!string.IsNullOrWhiteSpace(cursos.duracion) && !string.IsNullOrWhiteSpace(cursos.precio.ToString()) && !string.IsNullOrWhiteSpace(cursos.fecha_curso.ToString()) && cursos.id_vendedor > 0 && cursos.tipo_cursos != 0 && !string.IsNullOrWhiteSpace(cursos.descripcion))
        //        {
        //            if (cursos.id_cursos > 0)
        //            {
        //                CursosBO s = new CursosBO();
        //                if (cursos.imagen_http != null && cursos.imagen_http.ContentLength > 0)
        //                {
        //                    filename = Path.GetFileNameWithoutExtension(cursos.imagen_http.FileName);
        //                    extencion = Path.GetExtension(cursos.imagen_http.FileName);
        //                    filename = filename + DateTime.Now.ToString("yymmssfff") + extencion;
        //                    // cursos.id_vendedor = ViewBag.vista.id;
        //                    cursos.foto_curso = "~/imagenes/" + filename;
        //                    filename = Path.Combine(Server.MapPath("~/imagenes/"), filename);
        //                    cursos.imagen_http.SaveAs(filename);
        //                    curso.editar_curso(cursos);
        //                    ModelState.Clear();
        //                    ViewBag.resul = 2;

        //                }
        //                else
        //                {

        //                    curso.editar_curso(cursos);
        //                    ModelState.Clear();
        //                    ViewBag.resul = 2;

        //                }
        //            }
        //            else
        //            {
        //                filename = Path.GetFileNameWithoutExtension(cursos.imagen_http.FileName);
        //                extencion = Path.GetExtension(cursos.imagen_http.FileName);
        //                filename = filename + DateTime.Now.ToString("yymmssfff") + extencion;
        //                // cursos.id_vendedor = ViewBag.vista.id;
        //                cursos.foto_curso = "~/imagenes/" + filename;
        //                filename = Path.Combine(Server.MapPath("~/imagenes/"), filename);
        //                cursos.imagen_http.SaveAs(filename);


        //                curso.agregar(cursos);
        //                ModelState.Clear();
        //                ViewBag.resul = 1;
        //            }


        //            return View("agregar_curso", new CursosBO());
        //        }
        //        else
        //        {
        //            return View("agregar_curso", new CursosBO());
        //        }
        //    }

        //    return View("agregar_curso");

        //}




    }
}