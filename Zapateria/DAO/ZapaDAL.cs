﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Zapateria.Models;

namespace Zapateria.DAO
{
    public class ZapaDAL
    {
        ConexionDAO conexion = new ConexionDAO();
        public int agregar(object guardar)
        {

            Zapatos curso = (Zapatos)guardar;


            SqlCommand cmd = new SqlCommand("Insert into  [publicacion] ([titulo] ,[img],[precio] ,[contenido]) values (@titulo,@img,@precio,@contenido)");
          
            cmd.Parameters.Add("@titulo", SqlDbType.VarChar).Value = curso.Titulo;
            cmd.Parameters.Add("@img", SqlDbType.VarChar).Value = curso.Imagen;
            cmd.Parameters.Add("@precio", SqlDbType.VarChar).Value = curso.Precio;
            cmd.Parameters.Add("@contenido", SqlDbType.VarChar).Value = curso.Contenido;
           

            //int idcurso=  conexion.EjecutarComando(cmd);

            // notaBO nota = new notaBO();
            // SqlCommand comando = new SqlCommand("INSERT INTO notas  values(@nota,@fecha,@id_curso)");
            // comando.Parameters.Add("@fecha", SqlDbType.Date).Value = DateTime.Now.ToString("yyyy-MM-dd");
            // comando.Parameters.Add("@nota", SqlDbType.VarChar).Value = "Nota Agregada";
            // comando.Parameters.Add("@id_curso", SqlDbType.Int).Value = idcurso;
            return conexion.EjecutarComando(cmd);
        }
    }
}