﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Zapateria.Models;

namespace Zapateria.DAO
{
    public class ZapatosDAL
    {
        ConexionDAO conexion = new ConexionDAO();
        
        string connectionString = @"Data Source=DESKTOP-LMHEH3C\SQLEXPRESS;Initial Catalog=zapateria;Integrated Security=true;";

        public IEnumerable<Zapatos> ListarZapatos()
        {
           
            List<Zapatos> ListaZapatos = new List<Zapatos>();
            
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM [publicacion]", con);
                cmd.CommandType = CommandType.Text;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Zapatos oZapatos = new Zapatos();
                    oZapatos.ID = Convert.ToInt32(rdr["ID"]);
                    oZapatos.Titulo = rdr["titulo"].ToString();
                    oZapatos.Precio = rdr["Precio"].ToString();
                    oZapatos.Contenido = rdr["contenido"].ToString();
                    oZapatos.Imagen = rdr["img"].ToString();

                    ListaZapatos.Add(oZapatos);
                }
                con.Close();
            }
            return ListaZapatos;
        }
        public void AgregarZapato(Zapatos oZapatos)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO [publicacion] (titulo,contenido,Precio, img)VALUES(@Nombre,@Descripcion,@Precio, @Imagen)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@Nombre", oZapatos.Titulo);
                cmd.Parameters.AddWithValue("@Descripcion", oZapatos.Contenido);
                cmd.Parameters.AddWithValue("@Precio", oZapatos.Precio);
                cmd.Parameters.AddWithValue("@Imagen", oZapatos.Imagen);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        public void ModificarZapatos(Zapatos oZapatos)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("UPDATE [publicacion] SET titulo=@Nombre,contenido=@Descripcion,Precio=@Precio, img=@img Where ID=@ID", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ID", oZapatos.ID);
                cmd.Parameters.AddWithValue("@Nombre", oZapatos.Titulo);
                cmd.Parameters.AddWithValue("@Descripcion", oZapatos.Contenido);
                cmd.Parameters.AddWithValue("@Precio", oZapatos.Precio);
                cmd.Parameters.AddWithValue("@img", oZapatos.Imagen);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

            }
        }
        public Zapatos ObtenerDatosZapatos(int? ID)
        {
            Zapatos oZapatos = new Zapatos();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string sqlQuery = "SELECT * FROM [publicacion] WHERE ID= " + ID;
                SqlCommand cmd = new SqlCommand(sqlQuery, con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    oZapatos.ID = Convert.ToInt32(rdr["ID"]);
                    oZapatos.Titulo = rdr["titulo"].ToString();
                    oZapatos.Contenido = rdr["contenido"].ToString();
                    oZapatos.Precio = rdr["Precio"].ToString();
                    oZapatos.Imagen = rdr["img"].ToString();
                }
            }
            return oZapatos;
        }
        public void BorrarZapato(int? ID)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM [publicacion] WHERE ID=@ID", con);

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ID", ID);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }


    }

}