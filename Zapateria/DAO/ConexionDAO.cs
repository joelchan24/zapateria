﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Zapateria.DAO
{
    public class ConexionDAO
    {

        SqlCommand comandosql;
        SqlDataAdapter adaptador;
        DataSet datasetadaptador;
        SqlConnection coneccion;
        DataTable TablaVirtual;

        //DESKTOP-TT12AGM
        public ConexionDAO()
        {
            adaptador = new SqlDataAdapter();
            comandosql = new SqlCommand();
            coneccion = new SqlConnection();



        }


        public SqlConnection establecerConexion()
        {
            string cs = "Data Source=DESKTOP-LMHEH3C\\SQLEXPRESS;Initial Catalog=zapateria;Integrated Security=true;";
            //  string cs = "Data Source=DESKTOP-TT12AGM\\SQLEXPRESS; Initial catalog=crecon;   integrated security=true";
            //string cs = "Data Source=den1.mssql4.gear.host;Initial Catalog=crecon;Password=Gf8gEq16?1_0	;Integrated Security=False;User ID=crecon;";
            //string cs = "Data Source=SQL7003.site4now.net;Initial Catalog=DB_A3D865_creconBD;User Id=DB_A3D865_creconBD_admin;Password=joel12345;";

            coneccion = new SqlConnection(cs);
            return coneccion;
        }


        public void abrirConexion()
        {
            coneccion.Open();
        }
        public void cerrarConexion()
        {
            coneccion.Close();
        }
        public DataSet EjecutarSentencia(SqlCommand SqlComando)
        {


            // SELECT (Devolver registros)
            adaptador = new SqlDataAdapter();
            comandosql = new SqlCommand();
            datasetadaptador = new DataSet();

            comandosql = SqlComando;
            comandosql.Connection = this.establecerConexion();
            this.abrirConexion();
            adaptador.SelectCommand = comandosql;
            adaptador.Fill(datasetadaptador);
            this.cerrarConexion();
            return datasetadaptador;

        }
        public void backup()
        {



            string rutaa = @"C:\Inetpub\vhosts\crecon.com.mx\Crecon\backup\crecon.bak";


            if (System.IO.File.Exists(@"C:\Inetpub\vhosts\crecon.com.mx\Crecon\backup\crecon.bak"))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    System.IO.File.Delete(@"C:\Inetpub\vhosts\crecon.com.mx\Crecon\backup\crecon.bak");
                }
                catch (System.IO.IOException e)
                {

                }
            }

            SqlCommand SqlComando = new SqlCommand("BACKUP DATABASE crecon TO DISK = '" + rutaa + "'");
            adaptador = new SqlDataAdapter();
            comandosql = new SqlCommand();
            comandosql = SqlComando;
            comandosql.Connection = this.establecerConexion();

            this.abrirConexion();

            SqlComando.ExecuteNonQuery();
            this.cerrarConexion();





        }

        public int EjecutarComando(SqlCommand SqlComando)
        {
            // INSERT, DELETE, UPDATE
            comandosql = new SqlCommand();
            comandosql = SqlComando;
            comandosql.Connection = this.establecerConexion();
            this.abrirConexion();
            int id = 0; id = Convert.ToInt32(comandosql.ExecuteScalar());
            this.cerrarConexion();
            return id;

        }

        public DataTable TablaConsulta(string Sentencia)
        {
            adaptador = new SqlDataAdapter(Sentencia, establecerConexion());
            TablaVirtual = new DataTable();
            adaptador.Fill(TablaVirtual);
            return TablaVirtual;
        }
    }
}