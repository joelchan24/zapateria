﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Zapateria.Models;

namespace Zapateria.DAO
{
    public class LoginDAO
    {
        public UsuarioModel verificar(object consultar)
        {
            UsuarioModel usuario = (UsuarioModel)consultar;

            ConexionDAO con = new ConexionDAO();
            SqlCommand cmd = new SqlCommand("select *  from usuario where correo=@usuario and contra=@contra");
            cmd.Parameters.Add("@usuario", SqlDbType.VarChar).Value = usuario.correo;
            cmd.Parameters.Add("@contra", SqlDbType.VarChar).Value = usuario.contra;
            cmd.CommandType = CommandType.Text;
            DataSet valor1 = con.EjecutarSentencia(cmd)
                ;
            int resultado = valor1.Tables[0].Rows.Count;
            if (resultado != 0)
            {

                UsuarioModel usu = new UsuarioModel();
                usu.id = int.Parse(valor1.Tables[0].Rows[0][0].ToString());
                usu.nombre = valor1.Tables[0].Rows[0][1].ToString();
                usu.apellidoP = valor1.Tables[0].Rows[0][2].ToString();
                usu.apellidoM = valor1.Tables[0].Rows[0][3].ToString();
               
                usu.correo = valor1.Tables[0].Rows[0][8].ToString();
                usu.contra = valor1.Tables[0].Rows[0][7].ToString();
                

                 //  int resultado =Convert.ToInt32 (valor1.Tables[0].Rows[0][0].ToString()); ;



                // return (resultado != 0) ? true : false;
                return usu;
            }
            return usuario = null;


        }
      
    }
}