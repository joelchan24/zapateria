﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zapateria.Models
{
    public class Zapatos
    {
        public int ID { get; set; }
        public string Titulo { get; set; }
        public string Contenido { get; set; }
        public string Precio { get; set; }
        public string Imagen { get; set; }
        //public HttpPostedFileBase ImageFile { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

    }
}