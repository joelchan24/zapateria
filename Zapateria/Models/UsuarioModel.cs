﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zapateria.Models
{
    public class UsuarioModel
    {

        public int id { get; set; }
        public string nombre { get; set; }
        public string apellidoP { get; set; }
        public string apellidoM { get; set; }
        public string dir { get; set; }
        public string foto { get; set; }
        public string usuario { get; set; }
        public string contra { get; set; }
        public string correo { get; set; }
        

    }
}